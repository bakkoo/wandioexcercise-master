//
//  UITableViewCellExtension.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle.main)
    }
}
