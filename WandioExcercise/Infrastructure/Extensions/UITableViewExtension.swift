//
//  UITableViewExtension.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import UIKit

extension UITableView {
    func registerClass<T: UITableViewCell>(class: T.Type) {
        self.register(T.nib(), forCellReuseIdentifier: T.identifier)
    }
}
