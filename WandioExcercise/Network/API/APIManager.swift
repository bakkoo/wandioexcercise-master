//
//  APIManager.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation

class APIManager {
    
    func fetchData<T: Codable>(path: String, completion: @escaping (Result<T?, Error>) -> Void) {
      guard let unwrappedUrl = URL(string: Constants.Api.baseUrl + Constants.Api.Route.apiPath + path) else {return}
        URLSession.shared.dataTask(with: unwrappedUrl) { (data, response, error) in
         
            if let error = error {
                completion(.failure(error))
            }
            guard let unwrappedData = data else {return}
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: unwrappedData)
                completion(.success(decoded))
            } catch let err {
                print(err)
            }
        }.resume()
    }
}
