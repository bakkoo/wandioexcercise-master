//
//  Constants.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 12/24/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
// MARK: Animations
  struct AnimationOptions {
    static let standartDuration = 1.5
    static let standartDelay = 0.5
  }
// MARK: Network
  struct Api {
    static let baseUrl = "https://api.opendota.com"
    static let defaultLinkForImages = "https://api.opendota.com/api/matches"
    struct Route {
      static let apiPath = "/api"
      static let matchesPath = "/matches"
      static let teamsPath = "/teams"
    }
  }
}
