//
//  BaseViewController.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 12/24/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigationController()
  }
  
  func setupNavigationController() {
    let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 146, green: 165, blue: 37, alpha: 1.0)]
    self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    self.navigationController?.navigationBar.largeTitleTextAttributes = textAttributes
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    self.navigationItem.largeTitleDisplayMode = .automatic
    self.navigationController?.navigationBar.prefersLargeTitles = true
    self.navigationController?.navigationBar.isTranslucent = true
  }
}
