//
//  MatchIDModel.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation

struct MatchIDModel {
    
    var isRefreshing: ((Bool) -> Void)?
    
    private let apiManager: APIManager
    
    private let matchid: MatchID
    
    init(){
        apiManager = APIManager()
    }
    
    init(matchid: MatchID){
        self.matchid = matchid
    }
    
}





