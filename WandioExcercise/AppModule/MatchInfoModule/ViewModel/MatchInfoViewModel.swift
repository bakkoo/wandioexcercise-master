//
//  MatchInfoViewModel.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/16/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit
protocol MatchViewModelDelegate: class {
  func getTeamId(teamId: String)
  func getMatchDetails(matches: Matches)
}

class MatchViewModel {
  
  private var radiantWin: Bool?
  var matches: Matches?
  var delegate: MatchViewModelDelegate?
  func fetchMatches(path: String) {
    APIManager().fetchData(path: path) { [weak self] ( result: Result<Matches?, Error>) in
      switch result {
      case .success(let match):
        guard let match = match else {return}
        self?.matches = match
        self?.delegate?.getMatchDetails(matches: match)
        
      case .failure( _):
        print(WandioError.shared.errorMessage)
      }
    }
    
  }
  func setupTimeInterval(matches: Matches) -> String {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.minute, .second]
    formatter.unitsStyle = .positional
    let formattedString = formatter.string(from: TimeInterval(matches.duration ?? 0))!
    return formattedString
  }
  
  func getRadiantTeamDetailsByID() {
    if let radiantTeam = matches?.radiantTeam {
      delegate?.getTeamId(teamId: "\(radiantTeam.teamID ?? 0)")
    }
  }
  func getDireTeamDetailsByID() {
    if let direTeam = matches?.direTeam {
      delegate?.getTeamId(teamId: "\(direTeam.teamID ?? 0)")
    }
  }
  
}
