//
//  MatchInfoViewController.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/6/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit

class MatchInfoViewController: UIViewController {
  
  // MARK: IBs
  @IBOutlet private weak var gameMode: UILabel!
  @IBOutlet private weak var radiantVictory: UILabel!
  @IBOutlet private weak var scoreOfRadiant: UILabel!
  @IBOutlet private weak var scoreOfDire: UILabel!
  @IBOutlet private weak var timeOfMatch: UILabel!
  @IBOutlet private weak var radiantButton: UIButton!
  @IBOutlet private weak var direButton: UIButton!
  @IBOutlet private weak var radiantTeamIdLabel: UILabel!
  @IBOutlet private weak var direTeamIdLabel: UILabel!
  @IBOutlet private weak var spinner: UIActivityIndicatorView!
  
  // MARK: Vars
  var match: Matches?
  var matchID: String?
  var team: Team?
  private var viewModel = MatchViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureViewModel()
    addButtonTargets()
    configureNavigationController()
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    spinner.startAnimating()
    
  }
  func configureViewModel() {
    viewModel.delegate = self
    if let matchId = matchID {
      viewModel.fetchMatches(path: "\(Constants.Api.Route.matchesPath)/\(matchId)")
      self.navigationItem.title = "Match - \(matchID ?? "")"
    }
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)
    spinner.stopAnimating()
  }
  
  func addButtonTargets() {
    radiantButton.addTarget(self, action: #selector(radiantButtonClicked(sender:)), for: .touchUpInside)
    direButton.addTarget(self, action: #selector(direButtonClicked(sender:)), for: .touchUpInside)
  }
  func showDetailsOfTeam(teamId: String) {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyBoard.instantiateViewController(withIdentifier: "DetailOfTeams") as! TeamDetailsViewController
    vc.teamId = teamId
    vc.modalPresentationStyle = .fullScreen
    navigationController?.pushViewController(vc, animated: true)
  }
// MARK: Configuration
  func configureWinTeamsLabel(matches: Matches) {
    DispatchQueue.main.async { [self] in
      if matches.radiantWin ?? false {
        radiantVictory.text = "RADIANT VICTORY"
        radiantVictory.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
        scoreOfRadiant.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
        scoreOfDire.textColor = #colorLiteral(red: 0.7597871423, green: 0.2355121374, blue: 0.1642629206, alpha: 1)
      }else {
        radiantVictory.text = "DIRE VICTORY"
        radiantVictory.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
        scoreOfDire.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
        scoreOfRadiant.textColor = #colorLiteral(red: 0.7597871423, green: 0.2355121374, blue: 0.1642629206, alpha: 1)
      }
    }
  }
  func configureTeamScoreLabels(matches: Matches) {
    DispatchQueue.main.async { [self] in
      scoreOfDire.text = "\(matches.direScore ?? 0)"
      direButton.setTitle("\(matches.direTeam?.name ?? "")", for: .normal)
      scoreOfRadiant.text = "\(matches.radiantScore ?? 0)"
      radiantButton.setTitle("\(matches.radiantTeam?.name ?? "")", for: .normal)
      
      if matches.radiantWin ?? false {
        scoreOfDire.textColor = #colorLiteral(red: 0.7597871423, green: 0.2355121374, blue: 0.1642629206, alpha: 1)
        scoreOfRadiant.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
        direButton.setTitleColor(#colorLiteral(red: 0.7597871423, green: 0.2355121374, blue: 0.1642629206, alpha: 1), for: .normal)
        radiantButton.setTitleColor(#colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1), for: .normal)
      } else {
        scoreOfRadiant.textColor = #colorLiteral(red: 0.7597871423, green: 0.2355121374, blue: 0.1642629206, alpha: 1)
        scoreOfDire.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
        radiantButton.setTitleColor(#colorLiteral(red: 0.7597871423, green: 0.2355121374, blue: 0.1642629206, alpha: 1), for: .normal)
        direButton.setTitleColor(#colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1), for: .normal)
      }
    }
  }
  
  func configureTeamIdLabels(matches: Matches) {
    DispatchQueue.main.async { [self] in
      direTeamIdLabel.text = "\(matches.direTeam?.teamID ?? 0)"
      radiantTeamIdLabel.text = "\(matches.radiantTeam?.teamID ?? 0)"
      timeOfMatch.text = viewModel.setupTimeInterval(matches: matches)
    }
  }
  func configureGameMode(matches: Matches) {
    if let gamemode = GameMode(rawValue: matches.gameMode ?? 0){
      DispatchQueue.main.async {
        self.gameMode.text = gamemode.name
      }
    }
  }
  func configureNavigationController() {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain,
                                                       target: nil, action: nil)
    navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
  }
// MARK: ButtonClickHandle
  @objc func radiantButtonClicked(sender: UITapGestureRecognizer){
    viewModel.getRadiantTeamDetailsByID()
  }
  
  @objc func direButtonClicked(sender: UITapGestureRecognizer){
    viewModel.getDireTeamDetailsByID()
  }
  
}
// MARK: MatchViewModelDelegate
extension MatchInfoViewController: MatchViewModelDelegate {
  func getTeamId(teamId: String) {
    showDetailsOfTeam(teamId: teamId)
  }
  func getMatchDetails(matches: Matches) {
    DispatchQueue.main.async {
      self.spinner.stopAnimating()
    }
    configureGameMode(matches: matches)
    configureWinTeamsLabel(matches: matches)
    configureTeamScoreLabels(matches: matches)
    configureTeamIdLabels(matches: matches)
  }
  
}




