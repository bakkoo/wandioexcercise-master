//
//  Matches.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation

// MARK: - Matches

struct Matches: Codable {
    
    let gameMode: Int?
    let radiantWin: Bool?
    let duration: Int?
    let firstBloodTime: Int?
    let radiantScore: Int?
    let direScore: Int?
    let radiantTeam: Team?
    let direTeam: Team?
    
    enum CodingKeys: String, CodingKey {
        case gameMode = "game_mode"
        case radiantWin = "radiant_win"
        case duration
        case firstBloodTime = "first_blood_time"
        case radiantScore = "radiant_score"
        case direScore = "dire_score"
        case radiantTeam = "radiant_team"
        case direTeam = "dire_team"
    }
 
    
}

