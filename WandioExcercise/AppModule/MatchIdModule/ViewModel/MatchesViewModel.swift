//
//  MatchesViewModel.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit

protocol MatchesViewModelDelegate: class {
  func didSelectMatchId(matchId: String)
}

class MatchesViewModel {
  
  weak var delegate: MatchesViewModelDelegate?
  var matchids = ["5614025235","5616890858","5616857835","5616841810","5616838540","5616830606","5616800792","5616781007"]
  
  var numberOfRows: Int {
    return matchids.count
  }
  func didSelectRow(at indexPath: IndexPath) {
    if matchids.isEmpty { return }
    delegate?.didSelectMatchId(matchId: matchids[indexPath.row])
    
  }
  
}
