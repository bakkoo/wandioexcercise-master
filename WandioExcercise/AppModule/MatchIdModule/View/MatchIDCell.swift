//
//  MatchIDCell.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import UIKit

class MatchIDCell: UITableViewCell{
  @IBOutlet weak var matchIDLabel: UILabel!{
    didSet {
      matchIDLabel.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  internal func configure(with matchId: Int) {
    matchIDLabel.text = String(matchId)
  }
  
}
