//
//  ViewController.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/4/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import UIKit

class MatchIDViewController: BaseViewController {
  //MARK: IBs
  @IBOutlet weak var tableView: UITableView!
  
  //MARK: Vars
  private var viewModel = MatchesViewModel()
  var tableviewCell = MatchIDCell()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureTableView()
    configureViewModel()
    configureNavigationController()
  }
// MARK: Configuration
  private func configureTableView() {
    tableView.registerClass(class: MatchIDCell.self)
    tableView.dataSource = self
    tableView.delegate = self
    
  }
  func configureViewModel() {
    viewModel.delegate = self
  }
  func configureNavigationController() {
    navigationItem.title = "Matches"
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain,
                                                       target: nil, action: nil)
    navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
    
  }
  func showMatchDetails(matchId: String) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "MatchInfo") as! MatchInfoViewController
    vc.matchID = matchId
    vc.modalPresentationStyle = .fullScreen
    navigationController?.pushViewController(vc, animated: true)
  }
  
}

// MARK: UITableViewDataSource
extension MatchIDViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: MatchIDCell.identifier, for: indexPath) as? MatchIDCell else { return UITableViewCell() }
    cell.configure(with: Int(viewModel.matchids[indexPath.row]) ?? 0)
    
    return cell
  }
}

// MARK: UITableViewDelegate
extension MatchIDViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    viewModel.didSelectRow(at: indexPath)
  }
}
// MARK: ViewModelDelegate
extension MatchIDViewController: MatchesViewModelDelegate {
  func didSelectMatchId(matchId: String) {
    showMatchDetails(matchId: matchId)
  }
  
}

