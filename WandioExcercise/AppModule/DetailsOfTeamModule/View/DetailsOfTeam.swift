//
//  DetailsOfTeam.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/16/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit

class TeamDetailsViewController: UIViewController {
// MARK: IBs
  @IBOutlet private weak var winsNumber: UILabel!
  @IBOutlet private weak var lossesNumber: UILabel!
  @IBOutlet private weak var lastMatchTime: UILabel!
  @IBOutlet private weak var rating: UILabel!
  @IBOutlet private weak var nameOfTeam: UILabel!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
// MARK: Vars
  var team: DetailsOfTeam?
  var teamId: String?
  private var viewModel = TeamsInfoViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let teamID = teamId {
      viewModel.fetchMatches(path: "\(Constants.Api.Route.teamsPath)/\(teamID)")
      navigationItem.title = "Team - \(teamId ?? "")"
      
      spinner.stopAnimating()
    }
    configureViewModel()
    animations()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    spinner.startAnimating()
    viewStartPoints()
  }
// MARK: Configuration
  func configureViewModel() {
    viewModel.delegate = self
  }
  func configureNavigationController() {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain,
                                                       target: nil, action: nil)
    navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
  }
  func configureLabels(team: DetailsOfTeam) {
    DispatchQueue.main.async { [self] in
      lossesNumber.text = "\(team.losses ?? 0)"
      nameOfTeam.text = team.name
      winsNumber.text = "\(team.wins ?? 0)"
      winsNumber.textColor = #colorLiteral(red: 0.5725490196, green: 0.6470588235, blue: 0.1450980392, alpha: 1)
      lossesNumber.textColor = #colorLiteral(red: 0.7597871423, green: 0.2355121374, blue: 0.1642629206, alpha: 1)
      rating.text = "\(team.rating ?? 0.0)"
      lastMatchTime.text = viewModel.setupTimeInterval(team: team)
    }
  }
  func viewStartPoints() {
    winsNumber.center.x -= view.bounds.width
    lossesNumber.center.x -= view.bounds.width
    nameOfTeam.center.y -= view.bounds.height
    
  }
  func animations() {
    UIView.animate(withDuration: Constants.AnimationOptions.standartDuration,
                   delay: Constants.AnimationOptions.standartDelay,
                   usingSpringWithDamping: 0.6,
                   initialSpringVelocity: 0.7, options: [], animations: { [self] in
                    winsNumber.center.x += view.bounds.width
                    lossesNumber.center.x += view.bounds.width
                    nameOfTeam.center.y += view.bounds.height
    }, completion: nil)
  }
  
}
// MARK: ViewModelDelegate
extension TeamDetailsViewController: TeamsInfoViewModelDelegate {
  func getTeamsDetail(team: DetailsOfTeam) {
    DispatchQueue.main.async {
      self.spinner.stopAnimating()
    }
    configureLabels(team: team)
  }
  
}
