//
//  ConfigureViewModel.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/6/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit

class ConfigureViewModel {
    
    func configureLabel(matchIDLabel: UILabel){
        matchIDLabel.textColor = #colorLiteral(red: 0.6634734869, green: 0.8138934374, blue: 0.3291766644, alpha: 1)
    }
}
