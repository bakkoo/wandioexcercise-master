//
//  TeamsInfoViewModel.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/17/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit
protocol TeamsInfoViewModelDelegate {
  func getTeamsDetail(team: DetailsOfTeam)
}
class TeamsInfoViewModel {
  // MARK: Variables
  var team: DetailsOfTeam?
  var delegate: TeamsInfoViewModelDelegate?
  
  func setupTimeInterval(team: DetailsOfTeam) -> String {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.hour, .minute]
    formatter.unitsStyle = .positional
    let formattedString = formatter.string(from: TimeInterval(team.lastMatchTime ?? 0))!
    return formattedString
  }
  func fetchMatches(path: String) {
    APIManager().fetchData(path: path) { [weak self] ( result: Result<DetailsOfTeam?, Error>) in
      switch result {
      case .success(let team):
        guard let team = team else {return}
        self?.team = team
        self?.delegate?.getTeamsDetail(team: team)
      case .failure( _):
        print(WandioError.shared.errorMessage)
      }
    }
    
  }
}
