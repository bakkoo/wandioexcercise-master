//
//  DetailOfTeams.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 12/24/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation

// MARK: - DetailsOfTeam
struct DetailsOfTeam: Codable {
    
    let teamID: Int?
    let rating: Double?
    let wins: Int?
    let losses: Int?
    let lastMatchTime: Int?
    let name: String?
    let tag: String?
    
    enum CodingKeys: String,CodingKey {
        case teamID = "team_id"
        case rating
        case wins
        case losses
        case lastMatchTime = "last_match_time"
        case name = "name"
        case tag = "tag"
        
    }
}
