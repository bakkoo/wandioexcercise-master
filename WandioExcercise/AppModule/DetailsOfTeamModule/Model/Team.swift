//
//  Team.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 12/24/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation

// MARK: - Team
struct Team: Codable {
    let teamID: Int?
    let name, tag: String?
    let logoURL: String?
    
    enum CodingKeys: String, CodingKey {
        case teamID = "team_id"
        case name, tag
        case logoURL = "logo_url"
    }
}
