//
//  GameMode.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 9/20/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation
import UIKit

enum GameMode: Int {
    
    case game_mode_unknown = 0
    case game_mode_all_pick = 1
    case game_mode_captains_mode = 2
    case game_mode_random_draft = 3
    case game_mode_single_draft = 4
    case game_mode_all_random = 5
    case game_mode_intro = 6
    case game_mode_diretide = 7
    case game_mode_reverse_captains_mode = 8
    case game_mode_greeviling = 9
    case game_mode_tutorial = 10
    case game_mode_mid_only = 11
    case game_mode_least_played = 12
    case game_mode_limited_heroes = 13
    case game_mode_compendium_matchmaking = 14
    case game_mode_custom = 15
    case game_mode_captains_draft = 16
    case game_mode_balanced_draft = 17
    case game_mode_ability_draft = 18
    case game_mode_event = 19
    case game_mode_all_random_death_match = 20
    case game_mode_1v1_mid = 21
    case game_mode_all_draft = 22
    case game_mode_turbo = 23
    case game_mode_mutation = 24
    
    var name: String {
        switch self {
        case .game_mode_unknown:
            return "Unknown"
        case .game_mode_all_pick:
            return "All pick"
        case .game_mode_captains_mode:
            return "Captains Mode"
        case .game_mode_random_draft:
            return "Random Draft"
        case .game_mode_single_draft:
            return "Single Draft"
        case .game_mode_all_random:
            return "All Random"
        case .game_mode_intro:
            return "Intro"
        case .game_mode_diretide:
            return "Diretide"
        case .game_mode_reverse_captains_mode:
            return "Reverse Captains Mode"
        case .game_mode_greeviling:
            return "Greeviling"
        case .game_mode_tutorial:
            return "Tutorial"
        case .game_mode_mid_only:
            return "Mid Only"
        case .game_mode_least_played:
            return "Least Played"
        case .game_mode_limited_heroes:
            return "Limited Heroes"
        case .game_mode_compendium_matchmaking:
            return "Compendium Matchmaking"
        case .game_mode_custom:
            return "Custom"
        case .game_mode_captains_draft:
            return "Captains Draft"
        case .game_mode_balanced_draft:
            return "Balanced Draft"
        case .game_mode_ability_draft:
            return "Ability Draft"
        case .game_mode_event:
            return "Event"
        case .game_mode_all_random_death_match:
            return "All Random DeathMatch"
        case .game_mode_1v1_mid:
            return "1V1 Mid"
        case .game_mode_all_draft:
            return "All Draft"
        case .game_mode_turbo:
            return "Turbo"
        case .game_mode_mutation:
            return "Mutation"
            
        }
    }
}
