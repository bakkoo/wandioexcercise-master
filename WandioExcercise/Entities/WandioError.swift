//
//  WandioError.swift
//  WandioExcercise
//
//  Created by Bakur Khalvashi on 12/24/20.
//  Copyright © 2020 Bakur Khalvashi. All rights reserved.
//

import Foundation

struct WandioError: Error {
  let errorCode: Int
  let errorMessage: String
  static let shared = WandioError()
  init() {
    errorCode = 1
    errorMessage = "Error while processing data"
  }
    
}
